#!/usr/bin/env sh

cargo build

cbindgen --config cbindgen.toml --crate clean_insights_sdk --output example/clean_insights_sdk.h

rm -f example/example
gcc example/example.c -o example/example -lclean_insights_sdk -L./target/debug

LD_LIBRARY_PATH=./target/debug ./example/example
