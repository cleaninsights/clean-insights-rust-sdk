use crate::configuration::Configuration;
use crate::event::Event;
use crate::store::Store;
use crate::visit::Visit;

use chrono::{Utc, Duration};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Insights {

    /// Matomo site ID.
    pub idsite: u32,

    /// Preferred user languages as an HTTP Accept header.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lang: Option<String>,

    /// User Agent string.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ua: Option<String>,

    /// `Visit` data points.
    pub visits: Vec<Visit>,

    /// `Event` data points.
    pub events: Vec<Event>,
}

impl Insights {

    /// Create an `Insights` object according to configuration with all data from the store which is
    /// due for offloading to the server.
    ///
    /// # Arguments
    /// * conf: The current configuration.
    /// * store: The current measurement and consents store.
    /// * lang: User languages in order of preference.
    /// * ua: User Agent string.
    pub fn new(conf: &Configuration, store: &mut dyn Store, lang: &Vec<String>, ua: &Option<String>) -> Insights {

        Insights::purge(conf, store);

        let mut quality = 1.0;
        let mut lang_string = None;

        if !lang.is_empty() {
            let mut components = vec![];

            for l in lang {
                components.push(format!("{code};q={quality:.1}", code = l, quality = quality));

                quality -= 0.1;

                if quality < 0.5 {
                    break
                }
            }

            lang_string = Some(components.join(","));
        }

        let now = Utc::now();

        let visits = store.visits().iter().filter(|&v|
            conf.campaigns.contains_key(&v.campaign_id)
                // Only send, after aggregation period is over. `last` should contain that date!
                && now > v.last
        ).cloned().collect();

        let events = store.events().iter().filter(|&e|
            conf.campaigns.contains_key(&e.campaign_id)
                // Only send, after aggregation period is over. `last` should contain that date!
                && now > e.last
        ).cloned().collect();

        Insights { idsite: conf.site_id, lang: lang_string, ua: ua.clone(), visits, events }
    }

    pub fn is_empty(&self) -> bool {
        self.visits.is_empty() && self.events.is_empty()
    }

    /// Removes all visits and events from the given `Store`, which are also available in here.
    ///
    /// This should be called, when all `Insights` were offloaded at the server successfully.
    ///
    /// # Arguments
    /// * store: The store where the `Visit`s and `Event`s in here came from.
    pub fn clean(&self, store: &mut dyn Store) {
        store.visits_mut().retain(|v| !self.visits.contains(&v));

        store.events_mut().retain(|e| !self.events.contains(&e));
    }

    /// Removes `Visit`s and `Event`s, which are too old. These were never been sent, otherwise,
    /// they would have been removed, already.
    ///
    /// Remove them now, if they're over the threshold, to not accumulate too many `Visits` and
    /// `Events` and therefore reduce privacy.
    fn purge(conf: &Configuration, store: &mut dyn Store) {
        let threshold = Utc::now() - Duration::days(conf.max_age_of_old_data);

        store.visits_mut().retain(|v| v.last >= threshold);
        store.events_mut().retain(|e| e.last >= threshold);
    }
}

#[cfg(test)]
mod test {
    use crate::configuration::Configuration;
    use crate::consents::Consents;
    use crate::event::Event;
    use crate::insights::Insights;
    use crate::visit::Visit;
    use crate::store::Store;

    use std::error::Error;

    use chrono::{Utc, Duration};

    #[derive(Debug)]
    struct TestStore {

        visits: Vec<Visit>,

        events: Vec<Event>,
    }

    #[allow(unused_variables)]
    impl Store for TestStore {
        fn consents(&self) -> &Consents {
            todo!()
        }

        fn consents_mut(&mut self) -> &mut Consents {
            todo!()
        }

        fn visits(&self) -> &Vec<Visit> {
            &self.visits
        }

        fn visits_mut(&mut self) -> &mut Vec<Visit> {
            &mut self.visits
        }

        fn events(&self) -> &Vec<Event> {
            &self.events
        }

        fn events_mut(&mut self) -> &mut Vec<Event> {
            &mut self.events
        }

        fn persist(&self) -> Result<(), Box<dyn Error>> {
            todo!()
        }

        fn send(&self, data: String, server: &String, timeout: u64) -> Result<(), Box<dyn Error>> {
            todo!()
        }
    }

    #[test]
    fn purge() {
        let mut store = TestStore {
            visits: vec![],
            events: vec![]
        };

        let day_before_yesterday = Utc::now() - Duration::days(2);

        store.visits_mut().push(Visit {
            scene_path: vec!["foo".to_string()],
            campaign_id: "x".to_string(),
            times: 1,
            first: day_before_yesterday,
            last: day_before_yesterday
        });

        store.events_mut().push(Event {
            category: "foo".to_string(),
            action: "bar".to_string(),
            name: Some("baz".to_string()),
            value: Some(4567.0),
            campaign_id: "x".to_string(),
            times: 1,
            first: day_before_yesterday,
            last: day_before_yesterday
        });

        let conf = Configuration {
            server: "".to_string(),
            site_id: 0,
            timeout: 0,
            max_retry_delay: 0,
            max_age_of_old_data: 1,
            persist_every_n_times: 0,
            server_side_anonymous_usage: false,
            debug: false,
            campaigns: Default::default()
        };

        Insights::purge(&conf, &mut store);

        let v: Vec<Visit> = vec![];
        assert_eq!(store.visits(), &v);

        let e: Vec<Event> = vec![];
        assert_eq!(store.events(),&e);
    }
}