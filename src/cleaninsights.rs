use crate::campaign::{Campaign};
use crate::configuration::Configuration;
use crate::consents::{FeatureConsent, CampaignConsent, Feature, ConsentState, Consent};
use crate::default_store::DefaultStore;
use crate::event::Event;
use crate::insights::Insights;
use crate::store::Store;
use crate::visit::Visit;

use std::cmp::min;
use std::fs::File;
use std::io::BufReader;
use std::iter::FromIterator;
use std::path::Path;

use chrono::{DateTime, Utc, TimeZone, Duration};

#[derive(Debug)]
pub struct CleanInsights {

    /// The used configuration.
    pub conf: Configuration,

    /// User languages in order of preference.
    pub lang: Vec<String>,

    /// User Agent string.
    pub ua: Option<String>,

    store: Box<dyn Store>,

    persistence_counter: u32,

    sending: bool,

    failed_submission_count: u32,

    last_failed_submission: DateTime<Utc>,
}

impl CleanInsights {

    /// # Arguments
    /// * configuration: The Configuration provided as a `Configuration` object.
    /// * store: Your implementation of a `Store`.
    pub fn new(configuration: Configuration, store: Box<dyn Store>) -> CleanInsights {
        CleanInsights {
            conf: configuration, store, lang: vec![], ua: None,
            persistence_counter: 0, sending: false,
            failed_submission_count: 0, last_failed_submission: Utc.timestamp_opt(0, 0).unwrap()
        }
    }

    /// # Arguments
    /// * configuration: The Configuration provided as a `Configuration` object.
    /// * storage_dir: The location where to read and persist accumulated data.
    pub fn new_with_default_store(configuration: Configuration, storage_dir: &str) -> CleanInsights {
        let debug = configuration.debug;

        let store = DefaultStore::new(
            storage_dir, |message| CleanInsights::eprint_if_debug(debug, &message));

        CleanInsights::new(configuration, Box::new(store))
    }

    /// # Arguments
    /// * json_configuration_file: The Configuration provided as a URL to a JSON file
    ///     which can be deserialized to a `Configuration` object.
    /// * storage_dir: The location where to read and persist accumulated data.
    pub fn new_from_json_with_default_store(json_configuration_file: &str, storage_dir: &str) -> CleanInsights {
        CleanInsights::new_with_default_store(CleanInsights::read_conf(json_configuration_file), storage_dir)
    }

    /// # Arguments
    /// * json_configuration_file: The Configuration provided as a URL to a JSON file
    ///     which can be deserialized to a `Configuration` object.
    /// * store: Your implementation of a `Store`.
    pub fn new_from_json(json_configuration_file: &str, store: Box<dyn Store>) -> CleanInsights {
        CleanInsights::new(CleanInsights::read_conf(json_configuration_file), store)
    }

    /// Track a scene visit.
    ///
    /// * scene_path: A hierarchical path best describing the structure of your scenes. E.g. `['Main', 'Settings', 'Some Setting']`.
    /// * campaign_id: The campaign ID as per your configuration, where this measurement belongs to.
    pub fn measure_visit<T: AsRef<str>>(&mut self, scene_path: &[T], campaign_id: &str) {
        let scene_path: Vec<String> = scene_path.iter().map(|i| i.as_ref().to_string()).collect();

        let campaign = match self.get_campaign_if_good(&campaign_id, scene_path.join("/")) {
            None => {
                // Need to call anyway, otherwise last recorded data would never get sent!
                self.persist_and_send();

                return
            }
            Some(campaign) => campaign
        };

        let period = match campaign.current_measurement_period() {
            None => {
                self.debug("campaign.get_current_measurement_period() == None! This should not happen!");

                // Need to call anyway, otherwise last recorded data would never get sent!
                self.persist_and_send();

                return
            }
            Some(period) => period
        };

        let org_idx: Option<usize>;

        let visit = match self.store.visits().iter().find(|v|
            v.campaign_id == campaign_id
                && v.first >= period.0
                && v.first <= period.1
                && v.last >= period.0
                && v.last <= period.1
                && v.scene_path == scene_path
        ) {
            Some(v) => {
                org_idx = self.store.visits().iter().position(|x| x == v);

                let mut visit = v.clone();
                if !campaign.only_record_once {
                    visit.times += 1;
                }

                visit
            }
            None => {
                org_idx = None;
                Visit { scene_path, campaign_id: campaign_id.to_string(), times: 1, first: period.0, last: period.1 }
            }
        };

        // Remove originally found visit, if one was found and cloned before.
        if let Some(idx) = org_idx {
            self.store.visits_mut().remove(idx);
        }

        self.debug(&format!("Gain visit insight: {:?}", visit));

        self.store.visits_mut().push(visit);

        self.persist_and_send();
    }

    /// Track an event which doesn't need name and value parameters.
    ///
    /// * category: The event category. Must not be empty. (eg. Videos, Music, Games...)
    /// * action: The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
    /// * campaignId: The campaign ID as per your configuration, where this measurement belongs to.
    pub fn measure_simple_event(&mut self, category: &str, action: &str, campaign_id: &str) {
        self.measure_event(category, action, campaign_id, None, None)
    }

    /// Track an event.
    ///
    /// * category: The event category. Must not be empty. (eg. Videos, Music, Games...)
    /// * action: The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
    /// * campaignId: The campaign ID as per your configuration, where this measurement belongs to.
    /// * name: The event name. OPTIONAL.
    /// * value: The event value. OPTIONAL.
    pub fn measure_event(&mut self, category: &str, action: &str, campaign_id: &str, name: Option<String>, value: Option<f64>) {
        let campaign = match self.get_campaign_if_good(campaign_id, format!("{}/{}", category, action)) {
            None => {
                // Need to call anyway, otherwise last recorded data would never get sent!
                self.persist_and_send();

                return
            }
            Some(campaign) => campaign
        };

        let period = match campaign.current_measurement_period() {
            None => {
                self.debug("campaign.get_current_measurement_period() == None! This should not happen!");

                // Need to call anyway, otherwise last recorded data would never get sent!
                self.persist_and_send();

                return
            }
            Some(period) => period
        };

        let org_idx: Option<usize>;

        let event = match self.store.events().iter().find(|e|
            e.campaign_id == campaign_id
                && e.first >= period.0
                && e.first <= period.1
                && e.last >= period.0
                && e.last <= period.1
                && e.category == category
                && e.action == action
                && e.name == name
        ) {
            Some(e) => {
                org_idx = self.store.events().iter().position(|x| x == e);

                let mut event = e.clone();
                if !campaign.only_record_once {
                    event.times += 1;
                }

                campaign.apply(value, &mut event);

                event
            }
            None => {
                org_idx = None;
                Event {
                    name, value,
                    category: category.to_string(),
                    action: action.to_string(),
                    campaign_id: campaign_id.to_string(),
                    times: 1,
                    first: period.0,
                    last: period.1 }
            }
        };

        // Remove originally found event, if one was found and cloned before.
        if let Some(idx) = org_idx {
            self.store.events_mut().remove(idx);
        }

        self.debug(&format!("Gain event insight: {:?}", event));

        self.store.events_mut().push(event);

        self.persist_and_send();
    }

    #[no_mangle]
    pub extern "C" fn feature_consents_len(&self) -> usize {
        self.store.consents().features.len()
    }

    #[no_mangle]
    pub  extern "C" fn campaign_consents_len(&self) -> usize {
        self.store.consents().campaigns.len()
    }

    pub fn get_feature_consent_by_index(&self, index: usize) -> Option<FeatureConsent> {
        let features = &self.store.consents().features;

        if index >= features.len() {
            return None
        }

        let feature = Vec::from_iter(features.keys())[index];

        self.store.consents().consent_for_feature(*feature)
    }

    pub fn get_campaign_consent_by_index(&self, index: usize) -> Option<CampaignConsent> {
        let campaigns = &self.store.consents().campaigns;

        if index >= campaigns.len() {
            return None
        }

        let campaign_id = Vec::from_iter(campaigns.keys())[index];

        self.store.consents().consent_for_campaign(campaign_id)
    }

    pub fn grant_feature(&mut self, feature: Feature) -> FeatureConsent {
        let consent = self.store.consents_mut().grant_feature(feature);

        self.persist_and_send();

        consent
    }

    pub fn deny_feature(&mut self, feature: Feature) -> FeatureConsent {
        let consent = self.store.consents_mut().deny_feature(feature);

        self.persist_and_send();

        consent
    }

    /// Returns the consent for a given feature, if any available.
    ///
    /// * feature: The feature to check the consent state for.
    ///
    /// Returns the `FeatureConsent` for the given feature or `None`, if consent unknown.
    pub fn consent_for_feature(&self, feature: Feature) -> Option<FeatureConsent> {
        if self.conf.server_side_anonymous_usage {
            return Some(FeatureConsent::new(feature, &Consent::new(false)))
        }

        self.store.consents().consent_for_feature(feature)
    }

    /// Checks the consent state of a feature.
    ///
    /// * feature: The feature to check the consent state of.
    ///
    /// Returns the current state of consent.
    #[no_mangle]
    pub extern "C" fn state_of_feature(&self, feature: Feature) -> ConsentState {
        if self.conf.server_side_anonymous_usage {
            return ConsentState::Denied
        }

        self.store.consents().state_of_feature(feature)
    }

    pub fn grant_campaign(&mut self, campaign_id: &str) -> Option<CampaignConsent> {
        if !self.conf.campaigns.contains_key(campaign_id) {
            return None
        }

        let campaign = &self.conf.campaigns[campaign_id];

        let consent = self.store.consents_mut().grant_campaign(campaign_id, campaign);

        self.persist_and_send();

        Some(consent)
    }

    pub fn deny_campaign(&mut self, campaign_id: &str) -> Option<CampaignConsent> {
        if !self.conf.campaigns.contains_key(campaign_id) {
            return None
        }

        let consent = self.store.consents_mut().deny_campaign(campaign_id);

        self.persist_and_send();

        Some(consent)
    }

    /// Checks the consent state for a campaign.
    ///
    /// * campaign_id: The campaign ID to check the consent for.
    ///
    /// Returns `true`, if consent to run a campaign was given and is now valid.
    pub fn is_campaign_currently_granted(&self, campaign_id: &str) -> bool {
        self.state_of_campaign(campaign_id) == ConsentState::Granted
    }

    /// Returns the consent for a given campaign, if any available.
    ///
    /// * campaign_id: The campaign ID to check the consent state for.
    ///
    /// Returns the `CampaignConsent` for the given campaign or `None`, if consent unknown.
    pub fn consent_for_campaign(&self, campaign_id: &str) -> Option<CampaignConsent> {
        if self.conf.server_side_anonymous_usage {
            return Some(CampaignConsent::new(campaign_id.to_string(), &Consent::new(true)))
        }

        if !self.conf.campaigns.contains_key(campaign_id) {
            return None
        }

        if Utc::now() >= self.conf.campaigns[campaign_id].end {
            return None
        }

        self.store.consents().consent_for_campaign(campaign_id)
    }

    /// Checks the consent state of a campaign.
    ///
    /// * campaign_id: The campaign ID to check the consent state of.
    ///
    /// Returns the current state of consent.
    pub fn state_of_campaign(&self, campaign_id: &str) -> ConsentState {
        if self.conf.server_side_anonymous_usage {
            return ConsentState::Granted
        }

        if !self.conf.campaigns.contains_key(campaign_id) {
            return ConsentState::Unconfigured
        }

        if Utc::now() >= self.conf.campaigns[campaign_id].end {
            return ConsentState::Unconfigured
        }

        self.store.consents().state_of_campaign(campaign_id)
    }

    pub fn can_ask_consent_for_campaign(&self, campaign_id: &str) -> Option<(DateTime<Utc>, DateTime<Utc>)> {
        if !self.conf.campaigns.contains_key(campaign_id) {
            self.debug(&format!("Cannot request consent: Campaign '{}' not configured.", campaign_id));

            return None
        }

        let campaign = &self.conf.campaigns[campaign_id];

        if Utc::now() >= campaign.end {
            self.debug(&format!("Cannot request consent: End of campaign '{}' reached.", campaign_id));

            return None
        }

        if campaign.next_total_measurement_period().is_none() {
            self.debug(&format!("Cannot request consent: Campaign '{}' configuration seems messed up.", campaign_id));

            return None
        }

        if self.store.consents().campaigns.contains_key(campaign_id) {
            let consent = &self.store.consents().campaigns[campaign_id];

            let status = if consent.granted {
                format!("granted between {} and {}", consent.start, consent.end)
            } else {
                format!("denied on {}", consent.start)
            };

            self.debug(&format!("Already asked for consent for campaign '{}'. It was {}.", campaign_id, status));

            return None
        }

        self.conf.campaigns[campaign_id].next_total_measurement_period()
    }

    #[no_mangle]
    pub extern "C" fn can_ask_consent_for_feature(&self, feature: Feature) -> bool {
        if self.store.consents().features.contains_key(&feature) {
            let consent = &self.store.consents().features[&feature];

            self.debug(&format!("Already asked for consent for feature '{}'. It was {} on {}.",
                feature,
                if consent.granted { "granted" } else { "denied" },
                consent.start));

            return false
        }

        true
    }

    /// Persist accumulated data to the filesystem.
    ///
    /// The app should call this just before exit or when going into an idle mode.
    #[no_mangle]
    pub extern "C" fn persist(&mut self) {
        self.persist_private(true)
    }

    fn persist_private(&mut self, force: bool) {
        self.persistence_counter += 1;

        if force || self.persistence_counter >= self.conf.persist_every_n_times {
            match self.store.persist() {
                Ok(_) => {
                    self.persistence_counter = 0;

                    self.debug("Data persisted to storage.");
                }
                Err(e) => {
                    self.debug(&e.to_string());
                }
            }
        }
    }

    /// Persist data asynchronously and send all data to the CleanInsights Matomo Proxy server.
    ///
    /// If sending was successful, remove sent data from store and persist again.
    fn persist_and_send(&mut self) {
        self.persist_private(false);

        if self.sending {
            self.debug("Data sending already in progress.");

            return
        }

        self.sending = true;

        if self.failed_submission_count > 0 {
            // Calculate a delay for the next retry:
            // Minimum is 2 times the configured network timeout after the first failure,
            // exponentially increasing with number of retries.
            // Maximum is every conf.max_retry_delay interval.
            let exp = self.last_failed_submission + Duration::seconds((self.conf.timeout * 2_u64.pow(self.failed_submission_count)) as i64);
            let tru = self.last_failed_submission + Duration::seconds(self.conf.max_retry_delay);

            if Utc::now() < min(exp, tru) {
                self.sending = false;

                self.debug(&format!("Waiting longer to send data after {} failed attempts.", self.failed_submission_count));

                return
            }
        }

        let insights = Insights::new(&self.conf, &mut *self.store, &self.lang, &self.ua);

        if insights.is_empty() {
            self.sending = false;

            self.debug("No data to send.");

            return
        }

        let body = match serde_json::to_string(&insights) {
            Ok(body) => body,
            Err(e) => {
                self.last_failed_submission = Utc::now();
                self.failed_submission_count += 1;

                self.debug(&e.to_string());

                return
            }
        };

        match self.store.send(body, &self.conf.server, self.conf.timeout) {
            Ok(_) => {
                self.last_failed_submission = Utc.timestamp_opt(0, 0).unwrap();
                self.failed_submission_count = 0;

                self.debug("Successfully sent data.");

                insights.clean(&mut *self.store);

                self.persist_private(true);
            }
            Err(e) => {
                self.last_failed_submission = Utc::now();
                self.failed_submission_count += 1;

                self.debug(&e.to_string());
            }
        }
    }

    fn get_campaign_if_good(&self, campaign_id: &str, debug_string: String) -> Option<&Campaign> {
        if !self.conf.campaigns.contains_key(campaign_id) {
            self.debug(&format!("Measurement '{}' discarded, because campaign '{}' is missing in configuration.",
                               debug_string, campaign_id));
            return None
        }

        let campaign = &self.conf.campaigns[campaign_id];

        let now = Utc::now();

        if now < campaign.start {
            self.debug(&format!("Measurement '{}' discarded, because campaign '{}' didn't start, yet.",
                               debug_string, campaign_id));
            return None
        }

        if now > campaign.end {
            self.debug(&format!("Measurement '{}' discarded, because campaign '{}' already ended.",
                               debug_string, campaign_id));
            return None
        }

        if !self.is_campaign_currently_granted(campaign_id) {
            self.debug(&format!("Measurement '{}' discarded, because campaign '{}' has no user consent yet, any more or we're outside the measurement period.",
                               debug_string, campaign_id));
            return None
        }

        Some(campaign)
    }

    fn read_conf(conf_file: &str) -> Configuration {
        let path = Path::new(conf_file);

        let file = match File::open(path) {
            Ok(file) => file,
            Err(e) => panic!("Could not open '{}': {}", path.display(), e)
        };

        let reader = BufReader::new(file);

        match serde_json::from_reader(reader) {
            Ok(conf) => return conf,
            Err(e) => panic!("Could not read {}: {}", path.display(), e)
        };
    }

    fn debug(&self, message: &str) {
        CleanInsights::eprint_if_debug(self.conf.debug, message);
    }

    fn eprint_if_debug(debug: bool, message: &str) {
        if !debug {
            return
        }

        eprintln!("[CleanInsightsSDK] {}", message);
    }
}

impl Drop for CleanInsights {

    fn drop(&mut self) {
        self.persist();
    }

}
