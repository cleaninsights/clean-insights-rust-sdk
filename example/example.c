#include <stdio.h>
#include <string.h>

#include "clean_insights_sdk.h"

const char * state2str(ConsentState state) {
    switch(state) {
        case Unconfigured:
            return "Unconfigured";

        case Unknown:
            return "Unknown";

        case Denied:
            return "Denied";

        case NotStarted:
            return "NotStarted";

        case Expired:
            return "Expired";

        case Granted:
            return "Granted";
    }
}

const char * feature2str(Feature feature) {
    switch(feature) {
        case Lang:
            return "lang";

        case Ua:
            return "ua";
    }
}

void formatTs(char *buf, size_t bufsize, time_t time) {
    struct tm ts = *localtime(&time);

    strftime(buf, bufsize, "%Y-%m-%d %H:%M:%S %Z", &ts);
}

bool askPermission() {
    char answer;
    scanf("%c", &answer);
    fflush(stdin);

    return answer == 'y';
}

void ask_consent_campaign(CleanInsights *ci, const char *campaign_id) {
    Period *period = can_ask_consent_for_campaign(ci, campaign_id);

    if (!period) return;

    char start[80];
    formatTs(start, sizeof(start), period->start);

    char end[80];
    formatTs(end, sizeof(end), period->end);

    free_period(period);

    printf("We would like to run a measurement between %s and %s to understand some problems we noticed recently.\n\nYour help would be highly appreciated. [y/N]\n",
       start, end);

    if (askPermission()) {
        printf("Thank you!\n");
        grant_campaign(ci, campaign_id);
    }
    else {
        printf("Ok, no problem!\n");
        deny_campaign(ci, campaign_id);
    }
}

void ask_consent_feature(CleanInsights *ci, Feature feature) {
    if (!can_ask_consent_for_feature(ci, feature)) {
        return;
    }

    printf("In case you allow us to run measurements, are you ok that we record the following? %s [y/N]", feature2str(feature));

    if (askPermission()) {
        printf("Thank you!");
        grant_feature(ci, feature);
    }
    else {
        printf("Ok, no problem!");
        deny_feature(ci, feature);
    }
}


int main(void) {
    clock_t start = clock();

    CleanInsights *ci = new_clean_insights("example/cleaninsights.json", "tests");

    // This will only ever get used, when the user granted access to it and never in server mode.
    set_ua(ci, "Some Operating System. Could be formatted as a typical browser User-Agent string.");

    // This will only ever get use,d when the user granted access to it and never in server mode.
    const char *lang[] = {"en", "en-US", "en-GB"};
    set_lang(ci, lang, 3);

    ask_consent_campaign(ci, "test");

    ask_consent_feature(ci, Ua);

    ask_consent_feature(ci, Lang);

    ConsentState campaign_state = state_of_campaign(ci, "test");
    printf("state_of_campaign(\"test\")=\"%s\"\n", state2str(campaign_state));

    printf("feature_consents_len=%zu\n", feature_consents_len(ci));

    printf("campaign_consents_len=%zu\n", campaign_consents_len(ci));

    printf("is_campaign_currently_granted=%d\n", is_campaign_currently_granted(ci, "test"));

    ConsentState feature_state = state_of_feature(ci, Ua);
    printf("state_of_feature(\"ua\")=\"%s\"\n", state2str(feature_state));

    Consent *consent = consent_for_feature(ci, Ua);
    printf("consent_for_feature(\"ua\")->granted=%d\n", consent->granted);
    free_consent(consent);

    clock_t end = clock();
    double time = (double)(end - start) / CLOCKS_PER_SEC;

    char *name = "time needed";
    double *value = &time;

    measure_event(ci, "app-state", "startup-success", "test", name, value);

    const char *scene_path[] = {"Main"};
    measure_visit(ci, scene_path, 1, "test");

    // Possible anywhere.
    persist(ci);

    // Should persist on destruction automatically.
    free_clean_insights(ci);

    return 0;
}
