pub mod campaign;
pub mod cleaninsights;
pub mod configuration;
pub mod consents;
pub mod default_store;
pub mod event;
pub mod ffi;
pub mod insights;
pub mod store;
pub mod visit;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
