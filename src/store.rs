use crate::consents::Consents;
use crate::event::Event;
use crate::visit::Visit;

use std::error::Error;
use std::fmt::Debug;

/// The store holds the user's consents to the different `Feature`s and `Campaign`s, and their
/// `Visit` and `Event` measurements.
///
/// If you want to implement your own persistence of the store (e.g. because you want to write it in
/// a database  instead of the file system) and your own implementation of the transmission to the
/// Matomo/CIMP backend (e.g. because you want to tunnel the requests through a proxy or add your
/// own encryption layer), then create a subclass of this class and and implement the `#init`,
/// `#persist` and `#send` methods.
///
/// If you only want to change either one or the other, you can use `DefaultStore` as a base and
/// work from there.
pub trait Store: Debug {

    fn consents(&self) -> &Consents;

    fn consents_mut(&mut self) -> &mut Consents;

    fn visits(&self) -> &Vec<Visit>;

    fn visits_mut(&mut self) -> &mut Vec<Visit>;

    fn events(&self) -> &Vec<Event>;

    fn events_mut(&mut self) -> &mut Vec<Event>;

    /// This method gets called, when the SDK is of the opinion, that persisting the `Store` is in
    /// order.
    ///
    /// This is partly controlled by the `Configuration.persist_every_n_times` configuration option,
    /// and by calls to `CleanInsights#persist`.
    ///
    /// If no error is returned, the operation is considered successful and the internal counter
    /// will be set back again.
    fn persist(&self) -> Result<(), Box<dyn Error>>;

    /// This method gets called, when the SDK gathered enough data for a time period and is
    /// ready to send the data to a CIMP (CleanInsights Matomo Proxy).
    ///
    /// # Arguments
    /// * data: The serialized JSON for a POST request to a CIMP.
    /// * server: The server URL from `Configuration.server`.
    /// * timeout: The timeout in seconds from `Configuration.timeout`.
    ///
    /// If no error is returned, the data sent will be removed from the store and the store persisted.
    fn send(&self, data: String, server: &String, timeout: u64) -> Result<(), Box<dyn Error>>;
}
