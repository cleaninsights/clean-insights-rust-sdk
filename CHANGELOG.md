# Clean Insights SDK

## 2.3.0
- Replaced deprecated calls of latest chrono dependency.
- Added a C FFI.

## 2.2.0
 
- Added `consent_for_feature`, `consent_for_campaign` and `state_of_feature`, `state_of_campaign`
    methods to get detailed consent information.


## 2.1.1

- Fixed wrong JSON serialization of data sent to server.

## 2.1.0

- Relaxed anonymity guarantees somewhat by allowing to start measuring right away by default,
  not just at the next period. Added "strengthenAnonymity" toggle to switch back to old behaviour.
- Tested with updated dependencies.

## 2.0.1

- Fixed doc generation, publish to Gitlab pages and links to it.

## 2.0.0

- Initial working release. 
