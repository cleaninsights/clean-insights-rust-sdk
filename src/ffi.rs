mod ffi {
    use std::ffi::{c_char, CStr, CString};
    use libc::time_t;
    use chrono::{DateTime, Utc};
    use crate::cleaninsights::CleanInsights;
    use crate::consents::{CampaignConsent, ConsentState, Feature, FeatureConsent};

    #[repr(C)]
    pub struct Consent {
        pub granted: bool,
        pub start: time_t,
        pub end: time_t,
    }

    impl Consent {
        pub fn from_feature(consent: FeatureConsent) -> Consent {
            Consent {
                granted: consent.granted,
                start: consent.start.timestamp(),
                end: consent.end.timestamp(),
            }
        }

        pub fn from_campaign(consent: CampaignConsent) -> Consent {
            Consent {
                granted: consent.granted,
                start: consent.start.timestamp(),
                end: consent.end.timestamp(),
            }
        }
    }

    #[repr(C)]
    pub struct Period {
        pub start: time_t,
        pub end: time_t,
    }

    impl Period {
        pub fn new(period: (DateTime<Utc>, DateTime<Utc>)) -> Period {
            Period {
                start: period.0.timestamp(),
                end: period.1.timestamp(),
            }
        }
    }

    #[no_mangle]
    pub extern "C" fn new_clean_insights(json_configuration_file: *const c_char, storage_dir: *const c_char) -> Option<Box<CleanInsights>> {
        cchar_to_str(json_configuration_file)
            .and_then(|json_configuration_file| cchar_to_str(storage_dir)
                .and_then(|storage_dir| Some(Box::new(
                    CleanInsights::new_from_json_with_default_store(
                        json_configuration_file, storage_dir)))))
    }

    #[no_mangle]
    pub extern "C" fn set_ua(ci: &mut CleanInsights, ua: *const c_char) {
        ci.ua = CString::from(unsafe { CStr::from_ptr(ua) }).into_string().ok();
    }

    #[no_mangle]
    pub extern "C" fn set_lang(ci: &mut CleanInsights, lang: *const *const c_char, lang_size: usize) {
        ci.lang = cchar_array_to_vec(lang, lang_size).iter().map(|s| s.to_string()).collect();
    }

    #[no_mangle]
    pub extern "C" fn measure_visit(ci: &mut CleanInsights, scene_path: *const *const c_char, scene_path_size: usize, campaign_id: *const c_char) {
        let campaign_id = match cchar_to_str(campaign_id) {
            None => return,
            Some(it) => it
        };

        ci.measure_visit(&cchar_array_to_vec(scene_path, scene_path_size), campaign_id);
    }

    #[no_mangle]
    pub extern "C" fn measure_event(ci: &mut CleanInsights, category: *const c_char, action: *const c_char, campaign_id: *const c_char, name: *const c_char, value: *const f64) {
        let category = match cchar_to_str(category) {
            None => return,
            Some(it) => it
        };

        let action = match cchar_to_str(action) {
            None => return,
            Some(it) => it
        };

        let campaign_id = match cchar_to_str(campaign_id) {
            None => return,
            Some(it) => it
        };

        let name = cchar_to_str(name).map(|s| s.to_string());

        let value = unsafe { value.as_ref() }.cloned();

        ci.measure_event(category, action, campaign_id, name, value);
    }

    #[no_mangle]
    pub extern "C" fn get_feature_consent_by_index(ci: &CleanInsights, index: usize) -> Option<Box<Consent>> {
        ci.get_feature_consent_by_index(index)
            .and_then(|consent| Some(Box::new(Consent::from_feature(consent))))
    }

    #[no_mangle]
    pub extern "C" fn get_campaign_consent_by_index(ci: &CleanInsights, index: usize) -> Option<Box<Consent>> {
        ci.get_campaign_consent_by_index(index)
            .and_then(|consent| Some(Box::new(Consent::from_campaign(consent))))
    }

    #[no_mangle]
    pub extern "C" fn grant_feature(ci: &mut CleanInsights, feature: Feature) -> Box<Consent> {
        Box::new(Consent::from_feature(ci.grant_feature(feature)))
    }

    #[no_mangle]
    pub extern "C" fn deny_feature(ci: &mut CleanInsights, feature: Feature) -> Box<Consent> {
        Box::new(Consent::from_feature(ci.deny_feature(feature)))
    }

    #[no_mangle]
    pub extern "C" fn consent_for_feature(ci: &CleanInsights, feature: Feature) -> Option<Box<Consent>> {
        ci.consent_for_feature(feature)
            .and_then(|consent| Some(Box::new(Consent::from_feature(consent))))
    }

    #[no_mangle]
    pub extern "C" fn grant_campaign(ci: &mut CleanInsights, campaign_id: *const c_char) -> Option<Box<Consent>> {
        cchar_to_str(campaign_id)
            .and_then(|campaign_id| ci.grant_campaign(campaign_id)
                .and_then(|consent| Some(Box::new(Consent::from_campaign(consent)))))
    }

    #[no_mangle]
    pub extern "C" fn deny_campaign(ci: &mut CleanInsights, campaign_id: *const c_char) -> Option<Box<Consent>> {
        cchar_to_str(campaign_id)
            .and_then(|campaign_id| ci.deny_campaign(campaign_id)
                .and_then(|consent| Some(Box::new(Consent::from_campaign(consent)))))
    }

    #[no_mangle]
    pub extern "C" fn is_campaign_currently_granted(ci: &CleanInsights, campaign_id: *const c_char) -> bool {
        cchar_to_str(campaign_id)
            .and_then(|campaign_id| Some(ci.is_campaign_currently_granted(campaign_id)))
            .unwrap_or(false)
    }

    #[no_mangle]
    pub extern "C" fn consent_for_campaign(ci: &CleanInsights, campaign_id: *const c_char) -> Option<Box<Consent>> {
        cchar_to_str(campaign_id)
            .and_then(|campaign_id| ci.consent_for_campaign(campaign_id)
                .and_then(|consent| Some(Box::new(Consent::from_campaign(consent)))))
    }

    #[no_mangle]
    pub extern "C" fn state_of_campaign(ci: &CleanInsights, campaign_id: *const c_char) -> ConsentState {
        cchar_to_str(campaign_id)
            .and_then(|campaign_id| Some(ci.state_of_campaign(campaign_id)))
            .unwrap_or(ConsentState::Unknown)
    }

    #[no_mangle]
    pub extern "C" fn can_ask_consent_for_campaign(ci: &CleanInsights, campaign_id: *const c_char) -> Option<Box<Period>> {
        cchar_to_str(campaign_id)
            .and_then(|campaign_id| ci.can_ask_consent_for_campaign(campaign_id)
                .and_then(|period| Some(Box::new(Period::new(period)))))
    }

    //noinspection RsLiveness
    #[no_mangle]
    pub unsafe extern "C" fn free_clean_insights(_ci: Option<Box<CleanInsights>>) {
        // Dropped implicitly.
    }

    #[no_mangle]
    pub unsafe extern "C" fn free_consent(_ci: Option<Box<Consent>>) {
        // Dropped implicitly.
    }

    #[no_mangle]
    pub unsafe extern "C" fn free_period(_ci: Option<Box<Period>>) {
        // Dropped implicitly.
    }


    fn cchar_to_str(value: *const c_char) -> Option<&'static str> {
        unsafe {
            CStr::from_ptr(value)
        }.to_str().ok()
    }

    fn cchar_array_to_vec(array: *const *const c_char, size: usize) -> Vec<&'static str> {
        let mut out: Vec<&str> = vec![""; size];

        for i in 0..size {
            out[i] = unsafe {
                CStr::from_ptr(*array.offset(i as isize))
            }.to_str().unwrap_or("");
        }

        out
    }
}
