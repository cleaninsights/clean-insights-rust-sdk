use crate::campaign::Campaign;

use std::collections::HashMap;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter, Result};

#[repr(C)]
#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum Feature {
    #[serde(rename = "lang")]
    Lang,

    #[serde(rename = "ua")]
    Ua
}

impl Display for Feature {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            Feature::Lang => write!(f, "lang"),
            Feature::Ua => write!(f, "ua")
        }
    }
}

#[repr(C)]
#[derive(Debug, PartialEq)]
pub enum ConsentState {

    /// A campaign with that ID doesn't exist or already expired.
    Unconfigured,

    /// There's no record of consent. User was probably never asked.
    Unknown,

    /// User denied consent. Don't ask again!
    Denied,

    /// Consent was given, but consent period has not yet started.
    NotStarted,

    /// Consent was given, but consent period is over. You might ask again for a new period.
    Expired,

    /// Consent was given and is currently valid.
    Granted
}

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct Consent {

    pub granted: bool,

    pub start: DateTime<Utc>,

    pub end: DateTime<Utc>,
}

impl Consent {

    pub fn new(granted: bool) -> Consent {
        Consent { granted, start: Utc::now(), end: Utc::now() }
    }

    pub fn state(&self) -> ConsentState {
        if !self.granted {
            return ConsentState::Denied
        }

        ConsentState::Granted
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FeatureConsent {

    pub feature: Feature,

    pub granted: bool,

    pub start: DateTime<Utc>,

    pub end: DateTime<Utc>,
}

impl FeatureConsent {

    pub fn new(feature: Feature, consent: &Consent) -> FeatureConsent {
        FeatureConsent { feature, granted: consent.granted, start: consent.start, end: consent.end }
    }

    pub fn state(&self) -> ConsentState {
        if !self.granted {
            return ConsentState::Denied
        }

        ConsentState::Granted
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CampaignConsent {

    pub campaign_id: String,

    pub granted: bool,

    pub start: DateTime<Utc>,

    pub end: DateTime<Utc>,
}

impl CampaignConsent {

    pub fn new(campaign_id: String, consent: &Consent) -> CampaignConsent {
        CampaignConsent {
            campaign_id, granted: consent.granted, start: consent.start, end: consent.end }
    }

    pub fn state(&self) -> ConsentState {
        if !self.granted {
            return ConsentState::Denied
        }

        let now = Utc::now();

        if now < self.start {
            return ConsentState::NotStarted
        }

        if now > self.end {
            return ConsentState::Expired
        }

        ConsentState::Granted
    }
}

/// This struct keeps track of all granted or denied consents of a user.
///
/// There are two different types of consents:
/// - Consents for common features like if we're allowed to evaluate the locale or a user agent.
/// - Consents per measurement campaign.
///
/// The time of the consent is recorded along with it's state: If it was actually granted or denied.
///
/// Consents for common features are given indefinitely, since they are only ever recorded along
/// with running campaigns.
///
/// Consents for campaigns only last for a certain amount of days.
#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct Consents {

    pub features: HashMap<Feature, Consent>,

    pub campaigns: HashMap<String, Consent>,
}

impl Consents {

    /// User consents to evaluate a `Feature`.
    pub fn grant_feature(&mut self, feature: Feature) -> FeatureConsent {
        // Don't overwrite original grant timestamp.
        if !self.features.contains_key(&feature) || !self.features[&feature].granted {
            self.features.insert(feature, Consent::new(true));
        }

        let consent = &self.features[&feature];

        FeatureConsent::new(feature, consent)
    }

    /// User denies consent to evaluate a `Feature`.
    pub fn deny_feature(&mut self, feature: Feature) -> FeatureConsent {
        // Don't overwrite original deny timestamp.
        if !self.features.contains_key(&feature) || self.features[&feature].granted {
            self.features.insert(feature, Consent::new(false));
        }

        let consent = &self.features[&feature];

        FeatureConsent::new(feature, consent)
    }

    /// Returns the consent for a given feature, if any available.
    ///
    /// * feature: The feature to get the consent for.
    ///
    /// Returns the `FeatureConsent` for the given feature or `None`, if consent unknown.
    pub fn consent_for_feature(&self, feature: Feature) -> Option<FeatureConsent> {
        if !self.features.contains_key(&feature) {
            return None
        }

        Some(FeatureConsent::new(feature, &self.features[&feature]))
    }

    /// Checks the consent state of a feature.
    ///
    /// * feature: The feature to check the consent state of.
    ///
    /// Returns the current state of consent.
    pub fn state_of_feature(&self, feature: Feature) -> ConsentState {
        self.consent_for_feature(feature)
            .map_or(ConsentState::Unknown, |consent| consent.state())
    }

    /// User consents to run a specific campaign.
    ///
    /// * campaign_id: The campaign ID.
    /// * campaign: The campaign.
    pub fn grant_campaign(&mut self, campaign_id: &str, campaign: &Campaign) -> CampaignConsent {
        let period = campaign.next_total_measurement_period();

        match period {
            Some(period) => {
                // Always overwrite, since this might be a refreshed consent for a new period.
                self.campaigns.insert(campaign_id.to_string(), Consent { granted: true, start: period.0, end: period.1 });
            }
            None => {
                // Consent is technically granted, but has no effect, as start and end
                // will be set the same.
                self.campaigns.insert(campaign_id.to_string(), Consent::new(true));
            }
        }

        let consent = &self.campaigns[campaign_id];

        CampaignConsent::new(campaign_id.to_string(), consent)
    }

    /// User denies consent to run a specific campaign.
    pub fn deny_campaign(&mut self, campaign_id: &str) -> CampaignConsent {
        // Don't overwrite original deny timestamp.
        if !self.campaigns.contains_key(campaign_id) || self.campaigns[campaign_id].granted {
            self.campaigns.insert(campaign_id.to_string(), Consent::new(false));
        }

        let consent = &self.campaigns[campaign_id];

        CampaignConsent::new(campaign_id.to_string(), consent)
    }

    /// Returns the consent for a given campaign, if any available.
    ///
    /// * campaign_id: The campaign ID to get the consent for.
    ///
    /// the `CampaignConsent` for the given camapign or `None`, if consent unknown.
    pub fn consent_for_campaign(&self, campaign_id: &str) -> Option<CampaignConsent> {
        if !self.campaigns.contains_key(campaign_id) {
            return None
        }

        Some(CampaignConsent::new(campaign_id.to_string(), &self.campaigns[campaign_id]))
    }

    /// Checks the consent state of a campaign.
    ///
    /// * campaign_id: The campaign ID to check the consent state of.
    ///
    /// Returns the current state of consent.
    pub fn state_of_campaign(&self, campaign_id: &str) -> ConsentState {
        self.consent_for_campaign(campaign_id)
            .map_or(ConsentState::Unknown, |consent| consent.state())
    }
}
