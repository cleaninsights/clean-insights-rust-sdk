use clean_insights_sdk::cleaninsights::CleanInsights;
use clean_insights_sdk::consents::Feature;

use chrono::Utc;
use std::io::{stdin, stdout, Write};

fn main() {
    let start = Utc::now();

    let mut ci: CleanInsights = CleanInsights::new_from_json_with_default_store(
        "example/cleaninsights.json", "tests");

    // This will only ever get used, when the user granted access to it and never in server mode.
    ci.ua = Some("Some Operating System. Could be formatted as a typical browser User-Agent string.".to_string());

    // This will only ever get use,d when the user granted access to it and never in server mode.
    ci.lang = vec!["en".to_string(), "en-US".to_string(), "en-GB".to_string()];

    println!("{:?}", ci);

    ask_consent_campaign(&mut ci, "test");

    ask_consent_feature(&mut ci, Feature::Ua);

    ask_consent_feature(&mut ci, Feature::Lang);

    let time = Utc::now().timestamp_millis() - start.timestamp_millis();

    ci.measure_event("app-state", "startup-success", "test",
                     Some("time-needed".to_string()), Some(time as f64));

    println!("Hello World!");

    ci.measure_visit(&["Main"], "test");

    ci.persist();
}

fn ask_consent_campaign(ci: &mut CleanInsights, campaign_id: &str) {
    let period = match ci.can_ask_consent_for_campaign(campaign_id) {
        None => return,
        Some(period) => period
    };

    print!("We would like to run a measurement between {} and {} to understand some problems we noticed recently.\n\nYour help would be highly appreciated. [y/N]",
       period.0.format("%c"), period.1.format("%c"));
    stdout().flush().unwrap_or_else(|_| ());

    let mut answer = String::new();
    stdin().read_line(&mut answer).unwrap_or_else(|_| 0);

    if answer == "y\n" {
        println!("Thank you!");
        ci.grant_campaign(campaign_id);
    }
    else {
        println!("Ok, no problem!");
        ci.deny_campaign(campaign_id);
    }
}

fn ask_consent_feature(ci: &mut CleanInsights, feature: Feature) {
    if !ci.can_ask_consent_for_feature(feature) {
        return
    }

    print!("In case you allow us to run measurements, are you ok that we record the following? {} [y/N]", feature);
    stdout().flush().unwrap_or_else(|_| ());

    let mut answer = String::new();
    stdin().read_line(&mut answer).unwrap_or_else(|_| 0);

    if answer == "y\n" {
        println!("Thank you!");
        ci.grant_feature(feature);
    }
    else {
        println!("Ok, no problem!");
        ci.deny_feature(feature);
    }
}