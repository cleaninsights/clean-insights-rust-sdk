use crate::consents::Consents;
use crate::event::Event;
use crate::store::Store;
use crate::visit::Visit;

use std::error::Error;
use std::io::BufReader;
use std::fs::File;
use std::path::Path;
use std::time::Duration;

use serde::{Deserialize, Serialize};
use ureq;

#[derive(Debug, Deserialize, Serialize)]
/// Default implementation of a store. Stores the data in JSON format in a given folder.
///
/// The `#send` implementation uses the synchronous, blocking `ureq` HTTP client.
pub struct DefaultStore {

    consents: Consents,

    visits: Vec<Visit>,

    events: Vec<Event>,

    #[serde(skip_serializing, skip_deserializing)]
    storage_file: String,
}

impl DefaultStore {

    const STORAGE_FILENAME: &'static str = "cleaninsights.json";

    /// # Arguments
    /// * storage_dir: The location where to read and persist accumulated data.
    /// * debug: Function to output debug messages.
    pub fn new<F: Fn(String)>(storage_dir: &str, debug: F) -> DefaultStore {
        let storage_file = format!("{dir}/{file}", dir = storage_dir, file = DefaultStore::STORAGE_FILENAME);

        debug(format!("Create store in the following location: {}", storage_file));

        let empty_store = DefaultStore {
            storage_file: storage_file.clone(),
            consents: Consents { features: Default::default(), campaigns: Default::default() },
            visits: vec![],
            events: vec![] };

        let file = match File::open(Path::new(&storage_file)) {
            Ok(file) => file,
            Err(e) => {
                debug(e.to_string());

                return empty_store
            }
        };

        let reader = BufReader::new(file);

        let mut store: DefaultStore = match serde_json::from_reader(reader) {
            Ok(store) => store,
            Err(e) => {
                debug(e.to_string());

                empty_store
            }
        };

        debug("Data loaded from storage.".to_string());

        store.storage_file = storage_file;

        store
    }
}

impl Store for DefaultStore {

    fn consents(&self) -> &Consents {
        &self.consents
    }

    fn consents_mut(&mut self) -> &mut Consents {
        &mut self.consents
    }

    fn visits(&self) -> &Vec<Visit> {
        &self.visits
    }

    fn visits_mut(&mut self) -> &mut Vec<Visit> {
        &mut self.visits
    }

    fn events(&self) -> &Vec<Event> {
        &self.events
    }

    fn events_mut(&mut self) -> &mut Vec<Event> {
        &mut self.events
    }

    fn persist(&self) -> Result<(), Box<dyn Error>> {
        let file = File::create(&self.storage_file)?;

        match serde_json::to_writer(file, self) {
            Ok(_) => Ok(()),
            Err(e) => Err(Box::new(e))
        }
    }

    fn send(&self, data: String, server: &String, timeout: u64) -> Result<(), Box<dyn Error>> {
        match ureq::post(server)
            .timeout(Duration::from_secs(timeout))
            .set("Content-Type", "application/json; charset=UTF-8")
            .send_string(&data)?
            .into_string()
        {
            Ok(_) => Ok(()),
            Err(e) => Err(Box::new(e))
        }
    }
}